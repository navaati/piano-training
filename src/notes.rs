use std::fmt::Display;

use rand::prelude::*;

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum FloatingNote {
    Do,
    DoS,
    Re,
    ReS,
    Mi,
    Fa,
    FaS,
    Sol,
    SolS,
    La,
    LaS,
    Si,
}

impl FloatingNote {
    fn from_index(index: u8) -> Option<Self> {
        match index {
            0 => Some(FloatingNote::Do),
            1 => Some(FloatingNote::DoS),
            2 => Some(FloatingNote::Re),
            3 => Some(FloatingNote::ReS),
            4 => Some(FloatingNote::Mi),
            5 => Some(FloatingNote::Fa),
            6 => Some(FloatingNote::FaS),
            7 => Some(FloatingNote::Sol),
            8 => Some(FloatingNote::SolS),
            9 => Some(FloatingNote::La),
            10 => Some(FloatingNote::LaS),
            11 => Some(FloatingNote::Si),
            _ => None,
        }
    }
}

impl Display for FloatingNote {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            FloatingNote::Do => write!(f, "Do"),
            FloatingNote::DoS => write!(f, "Do#"),
            FloatingNote::Re => write!(f, "Ré"),
            FloatingNote::ReS => write!(f, "Ré#"),
            FloatingNote::Mi => write!(f, "Mi"),
            FloatingNote::Fa => write!(f, "Fa"),
            FloatingNote::FaS => write!(f, "Fa#"),
            FloatingNote::Sol => write!(f, "Sol"),
            FloatingNote::SolS => write!(f, "Sol#"),
            FloatingNote::La => write!(f, "La"),
            FloatingNote::LaS => write!(f, "La#"),
            FloatingNote::Si => write!(f, "Si"),
        }
    }
}

impl From<wmidi::Note> for FloatingNote {
    fn from(note: wmidi::Note) -> Self {
        match FloatingNote::from_index(u8::from(note) % 12) {
            Some(note) => note,
            None => unreachable!(),
        }
    }
}

impl Distribution<FloatingNote> for rand::distributions::Standard {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> FloatingNote {
        let r: u8 = rng.gen_range(0..12);
        match FloatingNote::from_index(r) {
            Some(note) => note,
            None => unreachable!(),
        }
    }
}

pub struct WhiteNotesDistribution;

impl Distribution<FloatingNote> for WhiteNotesDistribution {
    fn sample<R: Rng + ?Sized>(&self, rng: &mut R) -> FloatingNote {
        let r: u8 = rng.gen_range(0..7);
        match r {
            0 => FloatingNote::Do,
            1 => FloatingNote::Re,
            2 => FloatingNote::Mi,
            3 => FloatingNote::Fa,
            4 => FloatingNote::Sol,
            5 => FloatingNote::La,
            6 => FloatingNote::Si,
            _ => unreachable!(),
        }
    }
}
