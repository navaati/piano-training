use std::io::Write;
use std::ops::{Generator, GeneratorState};
use std::pin::Pin;

use rand::{prelude::*, distributions::Standard};

use crate::notes::*;

pub trait Exercise {
    fn progress(&mut self, note: FloatingNote) -> bool;
}

pub fn random_any_notes() -> impl Exercise {
    random_notes(Standard)
}

pub fn random_white_notes() -> impl Exercise {
    random_notes(WhiteNotesDistribution)
}

struct GeneratorExercise<'a>(Pin<Box<dyn Generator<FloatingNote, Yield = (), Return = ()> + 'a>>);

impl<'a> GeneratorExercise<'a> {
    fn new(generator: impl Generator<FloatingNote, Yield = (), Return = ()> + 'a) -> Self {
        GeneratorExercise(Box::pin(generator))
    }
}

impl Exercise for GeneratorExercise<'_> {
    fn progress(&mut self, note: FloatingNote) -> bool {
        match self.0.as_mut().resume(note) {
            GeneratorState::Yielded(_) => true,
            GeneratorState::Complete(_) => false,
        }
    }
}

fn random_notes<'a>(distribution: impl Distribution<FloatingNote> + 'a) -> impl Exercise + 'a {
    let mut rng = rand::thread_rng();

    let mut get_next_note = move || {
        let next_note = rng.sample(&distribution);
        print!("Play {}: ", next_note);
        let _ = std::io::stdout().flush();
        next_note
    };

    let mut next_note: FloatingNote = get_next_note();

    GeneratorExercise::new(move |mut note| {
        loop {
            print!("{} ", note);
            if note == next_note {
                println!("OK");
                next_note = get_next_note();
            } else {
                print!("KO. ");
                let _ = std::io::stdout().flush();
            }
            note = yield;
        }
    })
}
