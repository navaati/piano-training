#![feature(generators, generator_trait)]

use std::sync::mpsc;

use structopt::StructOpt;
use wmidi::MidiMessage;

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

mod jack_sender;
mod notes;
mod exercises;

#[derive(StructOpt)]
struct Config {
    #[structopt(subcommand)]
    exercise: ExerciseConfig,
}

#[derive(StructOpt)]
enum ExerciseConfig {
    Notes,
    Whites,
}

const CHANNEL_SIZE: usize = 64;

#[paw::main]
fn main(config: Config) -> Result<()> {
    //create a sync channel to send back copies of midi messages we get
    let (sender, receiver) = mpsc::sync_channel(CHANNEL_SIZE);

    let jack_sender = jack_sender::JackSender::new(sender.clone())?;

    let mut running_exercise = config.exercise.start();
    loop {
        let midi_message = receiver.recv()??;
        if let MidiMessage::NoteOn(_, raw_note, _) = midi_message {
            let note = notes::FloatingNote::from(raw_note);
            if !running_exercise.progress(note) {
                break
            }
        }
    }

    // optional deactivation
    jack_sender.deactivate()?;

    Ok(())
}

impl ExerciseConfig {
    fn start(&self) -> Box<dyn exercises::Exercise> {
        match self {
            ExerciseConfig::Notes => Box::new(exercises::random_any_notes()),
            ExerciseConfig::Whites => Box::new(exercises::random_white_notes()),
        }
    }
}
