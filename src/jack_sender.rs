use std::convert::TryInto;
use std::sync::mpsc;

use jack::*;
use wmidi::MidiMessage;

pub type JackResult = Result<MidiMessage<'static>, wmidi::FromBytesError>;

pub struct JackSender(AsyncClient<(), MyProcessHandler>);

struct MyProcessHandler {
    input: Port<MidiIn>,
    sender: mpsc::SyncSender<JackResult>,
}

impl ProcessHandler for MyProcessHandler {
    fn process(&mut self, _: &Client, ps: &ProcessScope) -> Control {
        for e in self.input.iter(ps) {
            let message = match e.bytes.try_into() {
                Ok(message) => {
                    let message: MidiMessage = message;
                    if let Some(message) = message.drop_unowned_sysex() {
                        Ok(message)
                    } else {
                        continue // Just ignore SysEx messages so we avoid allocating
                    }
                },
                Err(e) => Err(e),
            };
            // Ignore the result as being in a realtime thread there isn't much we can do, not even log
            let _ = self.sender.try_send(message);
        }
        jack::Control::Continue
    }
}

impl JackSender {
    pub fn new(sender: mpsc::SyncSender<JackResult>) -> crate::Result<JackSender> {
        let (client, _status) =
            jack::Client::new("piano-training", jack::ClientOptions::NO_START_SERVER)?;

        let input: Port<MidiIn> = client.register_port("midi_input", jack::MidiIn::default())?;

        let active_client = client.activate_async((), MyProcessHandler{ input, sender })?;

        Ok(JackSender(active_client))
    }

    pub fn deactivate(self: Self) -> crate::Result<()> {
        self.0.deactivate()?;
        Ok(())
    }
}
